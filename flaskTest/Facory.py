from abc import ABCMeta
from Adapters.AmazonSNS.SNSNotification import *
#Abstract Factory
class StandardFactory(object):

    @staticmethod
    def get_factory(factory):
        if factory == 'Email':
            return EmailFactory()
        elif factory == 'SMS':
            return SMSFactory()
        elif factory == 'Notification':
            return NotificationFactory()
        raise TypeError('Unknown Factory.')


#Factory
class EmailFactory(object):
    def getSender(self,type):
        if(type=="AmazonSES"):
            return AmazonSES()
        if(type=="OtherEmail"):
            return OtherEmail()


class SMSFactory(object):
    def getSender(self,type):
        if(type=="OurSMS"):
            return OurSMS();
        if(type=="OtherEmail"):
            return OtherSMS
class NotificationFactory(object):
    def getSender(self,type):
        if(type=="AmazonSNS"):
            return AmazonSNS()
        if(type=="GCM"):
            return  GCM()


# Product Interface
class Email(object):
    __metaclass__ = ABCMeta
    def play(self):
        pass
#Products
class AmazonSES(Email):
    def play(self):
        print ('AmazonSES callled')

class OtherEmail(Email):
    def play(self):
        print ('OtherMail callled')


class SMS(object):
    __metaclass__ = ABCMeta
    def play(self):
        pass

class OurSMS(SMS):
    def play(self):
        print ('OurSMS callled')

class OtherSMS(SMS):
    def play(self):
        print ('OtherSMS callled')


class Notification(object):
    __metaclass__ = ABCMeta
    def play(self):
        pass

class AmazonSNS(Notification):
    def play(self):
        print ('AmazonSNS callled')
    def send(self,list):
        print(list)
        print('send called')
        snsNotification=SNS()
        snsNotification.send(list)

class GCM(Notification):
    def play(self):
        print ('GCM callled')


# if __name__ =="__main__":
#     channel='Email'
#     if channel=='Email':
#         factory = StandardFactory.get_factory('Email')
#         email=factory.getSender("OtherEmail")
#         email.play()
#     factory = StandardFactory.get_factory('SMS')
#     sms=factory.getSender("OurSMS")
#     sms.play()
#
#     factory = StandardFactory.get_factory('Notification')
#     sns=factory.getSender("AmazonSNS")
#
#     list=[]
#     list.append({"device_token":"APA91bH-ViRTXvphWqQj_FNTvIUgTgO00Dd49x2XS0-XjkdWmoBivBSRssGgS0NrxWoqpPSzWDGYHRZQ5ywxocYxvnGpWuxcUML_bSDKFCOOZpIX82w7Or89EhNelJTJfUArV6sF7Bu2",
#                      "data":"{\"conuntry\":\"india\"}"})
#     sns.send(list)
