from flask import Flask,request
from Facory import *
from  Connection import *
app = Flask(__name__)
import json

@app.route('/')
def hello_world():
    print('hi you reached here')
    return 'hello'

@app.route('/messageReciever',methods=['GET','POST'])
def messageReciever():
    if request.method=='GET':
        return 'Contact Admin'
    if request.method=='POST':
        print('came to reciever')
        messageJson=request.get_json()
        #start of addition php message receiver
        import requests
        headers = {"Content-Type":"application/json"}
        phpUrl= 'http://54.191.89.126/messageReceiver.php'
        r = requests.post(phpUrl, data=messageJson,headers=headers)
        print('phpstatus',r.status)
        #end of addition php message receiver
        key=request.args.get('token')
        print('messageJson',messageJson['messages'])
        list=[]
        list=messageJson['messages']
        print('list recieved>>>>>>>>>>',list)
        count=0
        if messageJson['type']=='notification':
            factory = StandardFactory.get_factory('Notification')
            sns=factory.getSender("AmazonSNS")
            size=len(list)
            count=0
            sendlist=[]
            if(size>1000):
                while count<size:
                    if(size-count)>=1000:
                        sendlist=list[count,count+1000]
                        result=sns.send(sendlist)
                        print('result',result)
                    else:
                        result=sendlist=list[count,size]
                        print('result',result)
                        sns.send(sendlist)
                    count+=1000

            else:
                sns.send(list)




        #list.append({"device_token":"APA91bH-ViRTXvphWqQj_FNTvIUgTgO00Dd49x2XS0-XjkdWmoBivBSRssGgS0NrxWoqpPSzWDGYHRZQ5ywxocYxvnGpWuxcUML_bSDKFCOOZpIX82w7Or89EhNelJTJfUArV6sF7Bu2",
                     #"data":"{\"conuntry\":\"india\"}"})



        return '{"status":"success"}'


@app.route('/getStatus',methods=['GET'])
def getStatus():
    print('welcome to getStatus')
    con=Conn()
    data=[]
    data=con.statusSelect()
    print('data>>>>>> ',data)
    return json.dumps(data)

@app.route('/addEvent',methods=['POST'])
def addEvent():
    messageJson=request.get_json()
    list=[]
    list=messageJson['templates']
    con=Conn()

    con.addEvent(list)
    return 'success'

@app.route('/updateEvent',methods=['POST'])
def updateEvent():
    con=Conn()
    messageJson=request.get_json()
    con.updateEvent(messageJson)
    return 'clear'

@app.route('/deleteEvent',methods=['POST'])
def deleteEvent():
    con =Conn()
    messageJson=request.get_json()
    print('item>>>>>>',messageJson['Id'])
    con.removeEvent(messageJson['Id'])
    return 'deleted'

@app.route('/showEvent',methods=['POST'])
def showEvent():
    con=Conn()
    eventList=con.showEvent()
    return  json.dumps(eventList)





if __name__ == '__main__':
    app.debug=True
    app.run(host= '0.0.0.0')
