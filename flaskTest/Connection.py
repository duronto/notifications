import MySQLdb,traceback

class Conn:
    def beforeDeliverySelector(self,list):
        db = MySQLdb.connect("localhost","root","f","travelkhana" )
        query=self.createInsertQuery(list)
        print(query)
        cursor = db.cursor()
        print(query)
        data=''
        try:
            print(cursor.execute(query))
            db.commit()
        except:
            db.rollback()
            print('error')
        db.close()

    def createInsertQuery(self,list):
        query='insert into sns_status (device,status,sentDate) values'
        count=0
        for item in list:
            for key in item:
                if count<len(list)-1:
                    query=query+'(\''+str(key)+'\',\''+str(item[key])+'\',now()),'
                else:
                    query=query+'(\''+str(key)+'\',\''+str(item[key])+'\',now())'
                count+=1
        return query

    def statusSelect(self):
        db = MySQLdb.connect("localhost","root","f","travelkhana" )
        cursor = db.cursor()
        query='select * from sns_status;'
        statusList=[]
        try:
            cursor.execute(query)
            data=cursor.fetchall()
        except:
            print('error in retrieving data')
        for item in data:
            statusList.append({'arn':str(item[0]),'status':str(item[1]),'time':str(item[2].strftime("%Y-%m-%d %H:%M:%S"))})
        print(statusList)
        return {'data':statusList}

    def addEvent(self,list):
        db = MySQLdb.connect("localhost","root","f","travelkhana" )
        query='insert into message_template (event,email,notification,sms) values'
        count=0
        print('list>>>>>',list)
        for item in list:
                if count<len(list)-1:
                    query=query+'(\''+str(item['Event'])+'\',\''+str(item['Email'])+'\',\''+str(item['Notification'])+'\',\''+str(item['SMS'])+'\'),'
                else:
                    query=query+'(\''+str(item['Event'])+'\',\''+str(item['Email'])+'\',\''+str(item['Notification'])+'\',\''+str(item['SMS'])+'\');'
                count+=1
        print(query)
        cursor = db.cursor()
        try:
            print(cursor.execute(query))
            db.commit()
        except:
            db.rollback()
            print('error')
        db.close()

    def updateEvent(self,event):
        db = MySQLdb.connect("localhost","root","f","travelkhana" )
        query='update message_template set event=\''+str(event['Event'])+'\', email=\''+str(event['Email'])+'\',notification='+'\''+str(event['Notification'])+'\',sms=\''+str(event['SMS'])+'\' where eventId='+str(event['Id'])+';'
        print(query)
        cursor = db.cursor()
        try:
            print(cursor.execute(query))
            db.commit()
        except:
            db.rollback()
            print('error')
        db.close()
    def showEvent(self):
        db = MySQLdb.connect("localhost","root","f","travelkhana" )
        cursor = db.cursor()
        query='select * from message_template;'
        print(query)
        eventList=[]
        data=''
        try:
            cursor.execute(query)
            data=cursor.fetchall()
        except:
            print('error in retrieving data')
        for item in data:
            eventList.append({'Id':item[4],'Event':item[0],'Email':item[1],'Notication':item[2],'SMS':item[3]})
        print('eventList',eventList)
        return {'eventList':eventList}
    def removeEvent(self,id):
        db = MySQLdb.connect("localhost","root","f","travelkhana" )
        query='delete from message_template where eventId='+str(id)
        print(query)
        cursor = db.cursor()
        try:
            print(cursor.execute(query))
            db.commit()
        except:
            db.rollback()
            print('error')
        db.close()