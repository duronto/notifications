from gcmclient import *
from pprint import  pprint
# Pass 'proxies' keyword argument, as described in 'requests' library if you
# use proxies. Check other options too.
gcm = GCM("AIzaSyBb9Q7Ktv9HSy0F0hijVpcv0Dmhm0xZ2co")

# Construct (key => scalar) payload. do not use nested structures.
data = {'str': 'string', 'int': 10}

# Unicast or multicast message, read GCM manual about extra options.
# It is probably a good idea to always use JSONMessage, even if you send
# a notification to just 1 registration ID.
unicast = PlainTextMessage("APA91bF2wh-PMu7CG36tSfSe4BKY13hC8mSQKJov5trez8wuTIABmp4FXk-8cW_ZlKPujs1_94Ql6Gle1CM6_z38kMClUI_psE_v6JLmymCXgsRlZUtWxNKeIsMIPf4WDUy3unN3xg_q", data, dry_run=True)
multicast = JSONMessage(["registration_id_1", "registration_id_2"], data, collapse_key='my.key', dry_run=True)

# attempt send
res_unicast = gcm.send(unicast)
pprint(vars(res_unicast))

